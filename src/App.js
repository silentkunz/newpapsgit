import React from 'react';
import {Jumbotron, Button,} from "react-bootstrap";
import './App.css';
import ReactNotification from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'
import Main from './Router'

function App() {
  return (
    <div className="App">
        <Jumbotron style={{backgroundColor: '#3094ff'}}>
            <h1 style={{color: '#fff'}}>Project Manager</h1>
        </Jumbotron>
        <ReactNotification />
       <Main/>
    </div>
  );
}

export default App;
