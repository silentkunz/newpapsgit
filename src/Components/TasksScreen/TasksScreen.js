import React, {Component} from 'react';
import {Form, Button, Container, Row, ListGroup, FormControl, Col} from "react-bootstrap";
import {LinkContainer} from 'react-router-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import {
    projectsRequestPost,
    tasksRequest,
    tasksRequestPost,
    tasksRequestDelete,
    tasksRequestPut
} from "../../Networking/Request";
import {store} from "react-notifications-component";

class TasksScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: [],
            title: '',
            item: undefined,
        }
        this.updatedItems = [];
        this.onChange = e => this.setState({[e.target.name]: e.target.value})
    }

    componentDidMount() {
        console.log('PPWER', this.props.location.state.id)
        tasksRequest(this.props.location.state.id, (error, response = []) => {
            console.log(response)
            if (error) {
                alert(error);
            }
            this.setState({tasks: response})
            response.map((item) => {this.setState({item})});
        })
    }

    addTask = () => {
        const data = {
            title: this.state.title
        }
        tasksRequestPost(this.props.location.state.id, data, (error, response) => {
            if (error) {
                alert(error, null);
                store.addNotification({
                    title: "Task wasn't created",
                    message: "Please wait 2 seconds",
                    type: "danger",
                    insert: "top",
                    container: "bottom-center",
                    animationIn: ["animated", "fadeIn"],
                    animationOut: ["animated", "fadeOut"],
                    dismiss: {
                        duration: 2000,
                        onScreen: true
                    }
                });
            } else {
                console.log(response, 'yes');
                this.setState({
                    tasks: [...this.state.tasks, data]
                })
                store.addNotification({
                    title: "Task was created",
                    message: "Please wait 2 seconds",
                    type: "success",
                    insert: "top",
                    container: "bottom-center",
                    animationIn: ["animated", "fadeIn"],
                    animationOut: ["animated", "fadeOut"],
                    dismiss: {
                        duration: 2000,
                        onScreen: true
                    }
                });

            }
        });
    };

    deleteTask = () => {
        console.log('YPA', this.state)
        tasksRequestDelete(this.props.location.state.id, this.state.item.id, (error, response) => {
            if (error) {
                alert(error, null);
                store.addNotification({
                    title: "Task wasn't deleted",
                    message: "Please wait 2 seconds",
                    type: "danger",
                    insert: "top",
                    container: "bottom-center",
                    animationIn: ["animated", "fadeIn"],
                    animationOut: ["animated", "fadeOut"],
                    dismiss: {
                        duration: 2000,
                        onScreen: true
                    }
                });
            } else {
                console.log(response, 'yes');
                store.addNotification({
                    title: "Task was deleted",
                    message: "Please wait 2 seconds",
                    type: "success",
                    insert: "top",
                    container: "bottom-center",
                    animationIn: ["animated", "fadeIn"],
                    animationOut: ["animated", "fadeOut"],
                    dismiss: {
                        duration: 2000,
                        onScreen: true
                    }
                });
                setTimeout(() => {
                    window.location.reload();
                }, 3000)
            }
        });
    };

    changeTask = () => {
        console.log('STATE', this.state.item)
        const {id} = this.state.item;
        const data = {
            title: this.state.title
        }
        tasksRequestPut(this.props.location.state.id, id, data, (error, response) => {
            if (error) {
                alert(error, null);
                store.addNotification({
                    title: "Task wasn't changed",
                    message: "Please wait 2 seconds",
                    type: "danger",
                    insert: "top",
                    container: "bottom-center",
                    animationIn: ["animated", "fadeIn"],
                    animationOut: ["animated", "fadeOut"],
                    dismiss: {
                        duration: 2000,
                        onScreen: true
                    }
                });
            } else {
                console.log(response, 'yes');
                store.addNotification({
                    title: "Task was changed",
                    message: "Please wait 2 seconds",
                    type: "success",
                    insert: "top",
                    container: "bottom-center",
                    animationIn: ["animated", "fadeIn"],
                    animationOut: ["animated", "fadeOut"],
                    dismiss: {
                        duration: 2000,
                        onScreen: true
                    }
                });
            }
        });
    };

    render() {
        console.log(this.state);
        const {tasks} = this.state;

        if (tasks.length === 0) {
            return (
                <Container>
                    <h3>Tasks</h3>
                    <Row className="justify-content-center">
                        <ListGroup style={{flexDirection: 'row',}}>
                            <ListGroup.Item style={{flexDirection: 'row',}}>
                                <FormControl
                                    placeholder="Title"
                                    onChange={this.onChange}
                                    name="title"
                                />
                                <Button
                                    style={{marginTop: 10,}}
                                    variant="primary"
                                    type="submit"
                                    onClick={this.addTask}
                                >
                                    Add
                                </Button>
                            </ListGroup.Item>
                        </ListGroup>
                    </Row>
                    <div style={{marginTop: 20,}}>
                        <LinkContainer to={{
                            pathname: "/projects",
                        }}>
                            <Button variant="primary" type="submit">
                                Back
                            </Button>
                        </LinkContainer>
                    </div>
                </Container>
            )
        } else {
            return (
                <Container>
                    <h3>Tasks</h3>
                    <Row className="justify-content-center">
                        <ListGroup style={{flexDirection: 'column',}}>
                            {tasks.map((item) => (
                                <ListGroup.Item style={{flexDirection: 'column',}} key={item.id}>
                                    <FormControl
                                        placeholder="Title"
                                        onChange={this.onChange}
                                        name="title"
                                        type={"text"}
                                        defaultValue={item.title}
                                    />
                                    <Button
                                        style={{marginTop: 10, marginRight: 10,}}
                                        variant="primary"
                                        type="submit"
                                        onClick={this.changeTask}
                                    >
                                        Change
                                    </Button>
                                    <Button
                                        style={{marginTop: 10,}}
                                        variant="primary"
                                        type="submit"
                                        onClick={this.deleteTask}
                                    >
                                        Delete
                                    </Button>
                                </ListGroup.Item>
                            ))}
                            <ListGroup.Item style={{flexDirection: 'column',}}>
                                <FormControl
                                    placeholder="Title"
                                    onChange={this.onChange}
                                    type={"text"}
                                    name="title"
                                />
                                <Button
                                    style={{marginTop: 10,}}
                                    variant="primary"
                                    type="submit"
                                    onClick={this.addTask}
                                >
                                    Add
                                </Button>
                            </ListGroup.Item>
                        </ListGroup>
                    </Row>
                    <div style={{marginTop: 20,}}>
                        <LinkContainer to={{
                            pathname: "/projects",
                        }}>
                            <Button variant="primary" type="submit">
                                Back
                            </Button>
                        </LinkContainer>
                    </div>
                </Container>
            )
        }
    }
}

export default TasksScreen;
