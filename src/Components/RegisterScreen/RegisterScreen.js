import React, {Component} from 'react';
import {Form, Button, Container, Row} from "react-bootstrap";
import {LinkContainer} from 'react-router-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import {store} from "react-notifications-component";

class RegisterScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            confirmPassword: '',
            error: false,
            errorMessage: ''
        }
        this.onChange = e => this.setState({[e.target.name]: e.target.value})
    }

    checkPass = (e) => {
        const {password, confirmPassword, email} = this.state;
        if (password !== confirmPassword) {
            e.preventDefault();
            store.addNotification({
                title: "Пароли не совпадают",
                message: "Please wait 2 seconds",
                type: "danger",
                insert: "top",
                container: "bottom-center",
                animationIn: ["animated", "fadeIn"],
                animationOut: ["animated", "fadeOut"],
                dismiss: {
                    duration: 2000,
                    onScreen: true
                }
            });
        } else if (!password || !confirmPassword) {
            e.preventDefault();
            store.addNotification({
                title: "Введите пароль",
                message: "Please wait 2 seconds",
                type: "danger",
                insert: "top",
                container: "bottom-center",
                animationIn: ["animated", "fadeIn"],
                animationOut: ["animated", "fadeOut"],
                dismiss: {
                    duration: 2000,
                    onScreen: true
                }
            });
        } else if (!email) {
            e.preventDefault();
            store.addNotification({
                title: "Введите почту",
                message: "Please wait 2 seconds",
                type: "danger",
                insert: "top",
                container: "bottom-center",
                animationIn: ["animated", "fadeIn"],
                animationOut: ["animated", "fadeOut"],
                dismiss: {
                    duration: 2000,
                    onScreen: true
                }
            });
        }
    }

    render() {
        console.log(this.state)
        return (
            <Container>
                <h3>Sign Up</h3>
                <Row className="justify-content-md-center">
                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="Enter email"
                                name="email"
                                onChange={this.onChange}
                            />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password"
                                          placeholder="Password"
                                          name="password"
                                          onChange={this.onChange}
                            />
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Confirm password</Form.Label>
                            <Form.Control type="password"
                                          placeholder="Confirm password"
                                          name="confirmPassword"
                                          onChange={this.onChange}
                            />
                        </Form.Group>
                        {this.state.error &&
                        <Row className="justify-content-center"><a>{this.state.errorMessage}</a></Row>}
                        <Row className="justify-content-around">
                            <LinkContainer to={{
                                pathname: "/",
                                state: { email: this.state.email },
                            }}>
                                <Button variant="primary" type="submit" onClick={this.checkPass} onSubmit={e => { e.preventDefault(); }}>
                                    Submit
                                </Button>
                            </LinkContainer>
                            <LinkContainer to={{
                                pathname: "/",
                            }}>
                            <Button variant="primary" type="submit" >
                                Back
                            </Button>
                            </LinkContainer>
                        </Row>
                    </Form>
                </Row>
            </Container>
        )
    }
}

export default RegisterScreen;
