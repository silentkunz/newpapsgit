import React, {Component, useState} from 'react';
import {Col, Button, Container, Row, Card, Modal, FormControl} from "react-bootstrap";
import {LinkContainer} from 'react-router-bootstrap'
import {
    projectsRequest,
    projectsRequestDelete,
    projectsRequestPost,
} from "../../Networking/Request";
import {store} from 'react-notifications-component';
import './styles.css'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"

class ProjectsScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            projects: [],
            modal: false,
            title: '',
            item: undefined,
        }
        this.onChange = e => this.setState({[e.target.name]: e.target.value})
    }

    componentDidMount() {
        projectsRequest((error, response = []) => {
            console.log(response)
            if (error) {
                alert(error);
            }
            this.setState({projects: response})
            response.map((item) => {
                this.setState({item})
            });
        })
    }

    addProject = () => {
        const data = {
            title: this.state.title,
            image: 'https://pngimage.net/wp-content/uploads/2018/06/placeholder-png-5.png',
        }

        projectsRequestPost(data, (error, response) => {
            if (error) {
                alert(error, null);
                store.addNotification({
                    title: "Project wasn't created",
                    message: "Please wait 2 seconds",
                    type: "danger",
                    insert: "top",
                    container: "bottom-center",
                    animationIn: ["animated", "fadeIn"],
                    animationOut: ["animated", "fadeOut"],
                    dismiss: {
                        duration: 2000,
                        onScreen: true
                    }
                });
            } else {
                console.log(response, 'yes');
                this.setState({
                    projects: [...this.state.projects, data]
                })
                this.setState({modal: !this.state.modal})
                store.addNotification({
                    title: "Project was created",
                    message: "Please wait 2 seconds",
                    type: "success",
                    insert: "top",
                    container: "bottom-center",
                    animationIn: ["animated", "fadeIn"],
                    animationOut: ["animated", "fadeOut"],
                    dismiss: {
                        duration: 2000,
                        onScreen: true
                    }
                });
            }
        });
    };

    deleteProject = () => {
        console.log('YPA', this.state)
        projectsRequestDelete(this.state.item.id, (error, response) => {
            if (error) {
                alert(error, null);
                store.addNotification({
                    title: "Project wasn't deleted",
                    message: "Please wait 2 seconds",
                    type: "danger",
                    insert: "top",
                    container: "bottom-center",
                    animationIn: ["animated", "fadeIn"],
                    animationOut: ["animated", "fadeOut"],
                    dismiss: {
                        duration: 2000,
                        onScreen: true
                    }
                });
            } else {
                console.log(response, 'yes');
                store.addNotification({
                    title: "Project was deleted",
                    message: "Please wait 2 seconds",
                    type: "success",
                    insert: "top",
                    container: "bottom-center",
                    animationIn: ["animated", "fadeIn"],
                    animationOut: ["animated", "fadeOut"],
                    dismiss: {
                        duration: 2000,
                        onScreen: true
                    }
                });
                setTimeout(() => {
                    window.location.reload();
                }, 3000)
            }
        });
    };

    render() {
        console.log('EEE', this.props.location)
        const {projects} = this.state;
        if (projects.length === 0) {
            return (
                <Container>
                    <Modal show={this.state.modal} onHide={() => {
                        this.setState({modal: !this.state.modal})
                    }}>
                        <Modal.Header closeButton>
                            <Modal.Title>Create project</Modal.Title>
                        </Modal.Header>
                        <FormControl
                            placeholder="Title"
                            onChange={this.onChange}
                            name="title"
                        />
                        <Modal.Footer>
                            <Button variant="secondary" onClick={() => {
                                this.setState({modal: !this.state.modal})
                            }}>
                                Close
                            </Button>
                            <Button variant="primary" onClick={this.addProject}>
                                Create
                            </Button>
                        </Modal.Footer>
                    </Modal>
                    <h3 style={styles.h3}>Hello {this.props.location.state?.email}!</h3>
                    <div style={{marginBottom: 20,}}>
                        <LinkContainer to={{
                            pathname: "/",
                        }}>
                            <Button variant="primary" type="submit">
                                Log Out
                            </Button>
                        </LinkContainer>
                    </div>
                    <Row className="justify-content-center">
                        <Col xs="12" sm="4">
                            <Card style={{width: '18rem', marginBottom: '20px'}}>
                                <Card.Img variant="top"
                                          src={'https://pngimage.net/wp-content/uploads/2018/06/placeholder-png-5.png'}/>
                                <Button onClick={() => {
                                    this.setState({modal: !this.state.modal})
                                }} variant="primary">Create project</Button>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            )
        } else {
            return (
                <Container>
                    <Modal show={this.state.modal} onHide={() => {
                        this.setState({modal: !this.state.modal})
                    }}>
                        <Modal.Header closeButton>
                            <Modal.Title>Create project</Modal.Title>
                        </Modal.Header>
                        <div style={{padding: 10}}>
                            <FormControl
                                placeholder="Title"
                                onChange={this.onChange}
                                name="title"
                            />
                        </div>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={() => {
                                this.setState({modal: !this.state.modal})
                            }}>
                                Close
                            </Button>
                            <Button variant="primary" onClick={this.addProject}>
                                Create
                            </Button>
                        </Modal.Footer>
                    </Modal>
                    <h3 style={styles.h3}>Hello {this.props.location.state?.email}!</h3>
                    <div style={{marginBottom: 20,}}>
                    <LinkContainer to={{
                        pathname: "/",
                    }}>
                        <Button variant="primary" type="submit">
                            Log Out
                        </Button>
                    </LinkContainer>
                    </div>
                    <Row>
                        {projects.map((item) => (
                            <Col xs="12" sm="4" key={item.id}>
                                <Card style={{width: '18rem', marginBottom: '20px'}}>
                                    <Card.Img variant="top" src={item.image}/>
                                    <Card.Body>
                                        <Card.Title>{item.title}</Card.Title>
                                        <LinkContainer to={{
                                            pathname: "/tasks",
                                            state: {id: item.id},
                                        }}>
                                            <Button variant="primary" type="submit">
                                                Go to project
                                            </Button>
                                        </LinkContainer>
                                        <Button style={{marginLeft: 5,}} variant="primary" type="submit"
                                                onClick={this.deleteProject}>
                                            Delete
                                        </Button>
                                    </Card.Body>
                                </Card>
                            </Col>
                        ))}
                        <Col xs="12" sm="4">
                            <Card style={{width: '18rem', marginBottom: '20px'}}>
                                <Card.Img variant="top"
                                          src={'https://pngimage.net/wp-content/uploads/2018/06/placeholder-png-5.png'}/>
                                <Button onClick={() => {
                                    this.setState({modal: !this.state.modal})
                                }} variant="primary">Create project</Button>
                            </Card>
                        </Col>
                    </Row>

                </Container>
            )
        }
    }
}

const styles = {
    h3: {
        paddingBottom: 20,
    }
}

export default ProjectsScreen;
