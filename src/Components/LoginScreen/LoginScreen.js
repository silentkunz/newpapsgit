import React, {Component} from 'react';
import {Form, Button, Container, Row} from "react-bootstrap";
import {LinkContainer} from 'react-router-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import {store} from "react-notifications-component";

class LoginScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
        }
        this.onChange = e => this.setState({[e.target.name]: e.target.value})
    }

    checkPass = (e) => {
        const {password, email} = this.state;
     if (!email) {
            e.preventDefault();
            store.addNotification({
                title: "Введите почту",
                message: "Please wait 2 seconds",
                type: "danger",
                insert: "top",
                container: "bottom-center",
                animationIn: ["animated", "fadeIn"],
                animationOut: ["animated", "fadeOut"],
                dismiss: {
                    duration: 2000,
                    onScreen: true
                }
            });
        } else if (!password) {
          e.preventDefault();
          store.addNotification({
              title: "Введите пароль",
              message: "Please wait 2 seconds",
              type: "danger",
              insert: "top",
              container: "bottom-center",
              animationIn: ["animated", "fadeIn"],
              animationOut: ["animated", "fadeOut"],
              dismiss: {
                  duration: 2000,
                  onScreen: true
              }
          });
      }
    }

    render() {
        console.log(this.props);
        return (
            <Container>
                <h3>Sign In</h3>
                <Row className="justify-content-md-center">
                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email"  placeholder="Enter email" name="email" onChange={this.onChange}/>
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" name="password" onChange={this.onChange}/>
                        </Form.Group>
                        <Row className="justify-content-around">
                            <LinkContainer to={{
                                pathname: "/projects",
                                state: {email: this.state.email},
                            }}>
                                <Button variant="primary" type="submit" onClick={this.checkPass}>
                                    Submit
                                </Button>
                            </LinkContainer>
                            <LinkContainer to="/register">
                                <Button variant="primary" type="submit">
                                    Register
                                </Button>
                            </LinkContainer>
                        </Row>
                    </Form>
                </Row>
            </Container>
        )
    }
}

export default LoginScreen;
