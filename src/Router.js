import React from 'react';
import { Switch, Route, BrowserRouter as Router, } from 'react-router-dom'
import LoginScreen from "./Components/LoginScreen/LoginScreen";
import RegisterScreen from "./Components/RegisterScreen/RegisterScreen";
import ProjectsScreen from "./Components/ProjectsScreen/ProjectsScreen";
import TasksScreen from "./Components/TasksScreen/TasksScreen";

const Main = () => (
    <main>
        <Router>
        <Switch>
            <Route exact path='/' component={LoginScreen}/>
            <Route path='/register' component={RegisterScreen}/>
            <Route path='/projects' component={ProjectsScreen}/>
            <Route path='/tasks' component={TasksScreen}/>
        </Switch>
        </Router>
    </main>
)

export default Main;
