const baseUrl = 'http://5dc2b93b1666f6001477f3c5.mockapi.io';
const projectsUrl = baseUrl + '/Projects';
const tasks = baseUrl + '/tasks';
const deleteProjectUrl = (id) => projectsUrl + '/' + id;
const tasksUrl = (id) => projectsUrl + '/' + id + '/tasks';
const deleteTaskUrl = (id, taskId) => projectsUrl + '/' + id + '/tasks' + '/' + taskId;
const usersUrl = baseUrl + '/users';
const userProjectsUrl = (id) => baseUrl + '/users/' + id + '/Projects';
const userTasksUrl = (id) => userProjectsUrl(id) + '/tasks';

export {
    baseUrl,
    projectsUrl,
    usersUrl,
    userProjectsUrl,
    userTasksUrl,
    tasksUrl,
    tasks,
    deleteTaskUrl,
    deleteProjectUrl,
}
