import {apiRequest} from './Base';
import {projectsUrl, tasksUrl, usersUrl, tasks, deleteTaskUrl, deleteProjectUrl} from '../Constants/urls';

export const projectsRequest = (block) => {
    return apiRequest(projectsUrl, 'GET', null, block)
};

export const usersRequest = (block) => {
    return apiRequest(usersUrl, 'GET', null, block)
};

export const projectsRequestPost = (parameters, block) => {
    return apiRequest(projectsUrl, 'POST', parameters, block)
};

export const tasksRequest = (id, block) => {
    return apiRequest(tasksUrl(id), 'GET', null, block)
};

export const tasksRequestPost = (id, parameters, block) => {
    return apiRequest(tasksUrl(id), 'POST', parameters, block)
};

export const tasksRequestPut = (id, taskId, parameters, block) => {
    return apiRequest(deleteTaskUrl(id, taskId), 'PUT', parameters, block)
};

export const tasksRequestDelete = (id, taskId, block) => {
    return apiRequest(deleteTaskUrl(id, taskId), 'DELETE', null, block)
};

export const projectsRequestDelete = (projectId, block) => {
    return apiRequest(deleteProjectUrl(projectId), 'DELETE', null, block)
};

